export class Pokemon {
    name: string;
    url: string;
    image: string;
    backImage: string;
    height: string;
    weight: string;
    id: string;
}
