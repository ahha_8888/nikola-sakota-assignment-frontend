import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  base = "https://pokeapi.co/api/v2/";
  allPokemons = "pokemon";

  constructor(public httpClient: HttpClient) { }

  public getPokemonList(url: string) {
    return this.httpClient.get(url ? url : this.base + this.allPokemons).pipe(
      catchError( err => {
        return throwError(err);
      })
    );
  }

  public getPokemonDetails(url: string) {
    return this.httpClient.get(url).pipe(
      catchError( err => {
        return throwError(err);
      })
    );
  }
}
