import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PokemonsComponent } from './components/pokemons/pokemons.component';
import {RouterModule, Routes} from '@angular/router';
import {PokemonService} from './services/pokemon.service';
import {HttpClientModule} from '@angular/common/http';
import { PokemonComponent } from './components/pokemon/pokemon.component';
import { PokemonDetailsComponent } from './components/pokemon-details/pokemon-details.component';

const routes: Routes = [
  {path: '', component: PokemonsComponent},
  {path: 'pokemon-details/:id', component: PokemonDetailsComponent},
  {path: '**', component: PokemonsComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    PokemonsComponent,
    PokemonComponent,
    PokemonDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot(
        routes
    )
  ],
  providers: [PokemonService],
  bootstrap: [AppComponent]
})
export class AppModule { }
