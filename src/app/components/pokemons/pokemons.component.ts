import { Component, OnInit } from '@angular/core';
import {PokemonService} from '../../services/pokemon.service';
import {Pokemon} from '../../models/pokemon';
import {isUndefined} from 'util';

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemons.component.html',
  styleUrls: ['./pokemons.component.scss']
})
export class PokemonsComponent implements OnInit {

  pokemons: Pokemon[] = [];
  nextUrl = '';

  constructor(public pokemonService: PokemonService) { }

  ngOnInit() {
    this.goToNextPage();
  }
  goToNextPage() {
    this.pokemons = [];
    this.pokemonService.getPokemonList(this.nextUrl).subscribe(data => {

      this.nextUrl = data['next'];

      data['results'].map(pokemon =>
          this.pokemons.push(pokemon)
      );
    });
  };

}
