import {Component, Input, OnChanges} from '@angular/core';
import {Pokemon} from '../../models/pokemon';
import {PokemonService} from '../../services/pokemon.service';
import {log} from 'util';
import {Router} from '@angular/router';

@Component({
  selector: 'pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.scss']
})
export class PokemonComponent implements OnChanges {

  @Input() url : string;

  pokemon : Pokemon = new Pokemon();

  constructor(public pokemonService: PokemonService, public router: Router) { }

  ngOnChanges() {
    this.pokemonService.getPokemonDetails(this.url).subscribe(data => {
      this.pokemon.url = this.url;
      this.pokemon.name = data['name'];
      this.pokemon.image = data['sprites'].front_default;
    });
  }

  showDetails() {
    let withoutLastSlash = this.url.substring(0, this.url.length - 1);
    let id = withoutLastSlash.substr(withoutLastSlash.lastIndexOf('/') + 1);
    this.router.navigate(['pokemon-details', id]);
  }
}

