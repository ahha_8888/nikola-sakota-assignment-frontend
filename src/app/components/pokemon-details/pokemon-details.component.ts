import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PokemonService} from '../../services/pokemon.service';
import {Pokemon} from '../../models/pokemon';
import {log} from "util";

@Component({
  selector: 'app-pokemon-details',
  templateUrl: './pokemon-details.component.html',
  styleUrls: ['./pokemon-details.component.scss']
})
export class PokemonDetailsComponent implements OnInit {

  baseUrl = "https://pokeapi.co/api/v2/pokemon/";

  constructor(private pokemonService: PokemonService, private route: ActivatedRoute) { }

  pokemon: Pokemon = new Pokemon();

  ngOnInit() {
    const id  = this.route.snapshot.paramMap.get('id');
    this.getPokemon(id);
  }

  getPokemon(id: string) {
    this.pokemonService.getPokemonDetails(this.baseUrl + id).subscribe(data => {
      this.pokemon.url = this.baseUrl + id;
      this.pokemon.name = data['name'];
      this.pokemon.image = data['sprites'].front_default;
      this.pokemon.backImage = data['sprites'].back_default;
      this.pokemon.height = data['height'];
      this.pokemon.weight = data['weight'];
    });
  }

}
