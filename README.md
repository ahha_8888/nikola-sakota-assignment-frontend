# Assignment Frontend :sunglasses:

Hi, your goal is to write a site showcasing all pokemon's that have ever come to existence. The app will be a SPA written in Angular 7. In order to complete the assignment you need to fetch all the pokemon's from the api and then display them in a list to the user.

We recommend that you spend 2-3 hours on this assignment. Start by forking this repo, once you're done send us your link to your repository with the completed assignment.

The fork button can be a little hard to find in BitBucket. But here are some images of the steps to take to find it

![step1](/docs/s1.png)
![step2](/docs/s2.png)

## API
The api to gather the pokemon's are located here: [https://www.pokeapi.co/](https://www.pokeapi.co/). We recommend that you start by getting accustomed to it's endpoints and from there start deciding on how you want to layout your app.

## Goals
**Required goals**

* Fetch pokemon's from the pokemon api
* Display a list of pokemon's to the user
* Some level of styling to the list

**Stretch goals**

* Proper error handling
* Testing of key points of the app
* Utilizing sass (variables etc)


Remember! This is your app and your chance to show us what you're made of! May you live long and prosper :spock-hand:

## Getting Started

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
